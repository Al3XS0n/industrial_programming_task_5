import Navigation from "../Components/Navigation";
import { YMaps } from "react-yandex-maps";
import Ymap from "../Components/Ymap";
import { YMAPS_APIKEY } from "../Tools/API";

export default function Map() {
    return (
        <>
            <Navigation />
            <div style={{position: "relative", height: "800px"}} className="content">
                <YMaps query={{apikey: YMAPS_APIKEY}}>
                    <Ymap />
                </YMaps>
            </div>
        </>
    );
}