import Navigation from "../Components/Navigation";
import "./Calculator.css";
import PriceEstimator from "../Components/PriceEstimator";

export default function Calculator() {
    return (
        <>
            <Navigation />
            <div className="content">
                <PriceEstimator></PriceEstimator>
            </div>
        </>
    );
}