import Navigation from "../Components/Navigation";
import PlacementList from "../Components/PlacementList";

export default function Home() {
    return (
        <>
            <Navigation />
            <PlacementList />
        </>
    );
}