import React from "react";
import { Routes, Route } from "react-router-dom";
import Home from "./Pages/Home";
import Calculator from "./Pages/Calculator";
import Map from "./Pages/Map";
import "normalize.css";
import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/icons/lib/css/blueprint-icons.css";

function App() {
  return (
    <Routes>
        <Route path="/" element={<Home></Home>}/>
        <Route path="/map" element={<Map></Map>}/>
        <Route path="/placements" element={<Home></Home>}/>
        <Route path="/calculator" element={<Calculator></Calculator>}/>
    </Routes>
  );
}

export default App;
