import React from "react";
import GoogleMapReact from 'google-map-react';

const AnyReactComponent = ({ text }) => <div>{text}</div>;

export default function Gmap(){
  const defaultProps = {
    center: {
      lat: 55.751244,
      lng: 37.618423
    },
    zoom: 10
  };

  return (
    // Important! Always set the container height explicitly
    <div style={{ height: '60vh', width: '100%' }}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: "" }}
        defaultCenter={defaultProps.center}
        defaultZoom={defaultProps.zoom}
      >
        <AnyReactComponent
          lat={55.751244}
          lng={37.618423}
          text="Test marker"
        />
      </GoogleMapReact>
    </div>
  );
}