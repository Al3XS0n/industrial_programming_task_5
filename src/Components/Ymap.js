import { Map, Placemark, Clusterer } from "react-yandex-maps";

export default function Ymap(props) {
  const style = {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%'
  };
  return (
      <div>
        <Map defaultState={{ center: [55.75, 37.57], zoom: 10 }} style={style}>
          <Clusterer
            options={{
              preset: "islands#invertedVioletClusterIcons",
              groupByCoordinates: false,
            }}
          >
            <Placemark geometry={[55.684758, 37.738521]} properties={{iconContent: 'Метка с длинным названием'}} options={{preset: 'islands#nightStretchyIcon'}}/>
          </Clusterer>
        </Map>
      </div>
  );
}