import React from "react";
import "./Navigation.css";
import { Alignment, Navbar,  NavbarDivider,  NavbarGroup, NavbarHeading } from "@blueprintjs/core";
import { AnchorButton } from "@blueprintjs/core";

export default function Navigation() {
    return (
        <Navbar >
            <NavbarGroup className={Alignment.LEFT}>
                <NavbarHeading>
                    Система оценки стоимости недвижемости
                </NavbarHeading>

                <NavbarDivider></NavbarDivider>

                <AnchorButton
                    href="/map"
                    text="Карта"
                    // target="_blank"
                    minimal
                    large
                    rightIcon="map"
                />

                <AnchorButton 
                    href="/placements"
                    text="Объявления"
                    // target="_blank"
                    minimal
                    large
                    rightIcon="property"
                />

                <AnchorButton 
                    href="/calculator"
                    text="Оценка стоимости"
                    // target="_blank"
                    minimal
                    large
                    rightIcon="Calculator"
                />
            </NavbarGroup>
        </Navbar>
    );
};