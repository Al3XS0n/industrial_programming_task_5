import { Button, Classes, Divider, FormGroup, H4, InputGroup, NumericInput } from "@blueprintjs/core";
import { useState } from "react";
import React from "react";
import "./PriceEstimator.css";
import { YMaps, Map } from "react-yandex-maps";
import { YMAPS_APIKEY } from "../Tools/API";

 
const mapState = {
    center: [55.76, 37.64],
    zoom: 10,
    controls: []
};

function valid_number(value) {
    return (value > 0 ? value : 0);
}

function valid_boolean(value) {
    if (value > 1 || value < 0) {
        return 0;
    }
    return value;
}

export default function PriceEstimator(props) {
    const [rooms, setRooms] = useState(0);
    const [area, setArea] = useState(0);
    const [floor, setFloor] = useState(0);
    const [gas, setGas] = useState(0);
    const [elev, setElev] = useState(0);
    const [balcony, setBalcony] = useState(0);
    const [address, setAddress] = useState("Адрес");
    const [estPrice, setEstPrice] = useState(0);
    const [coords, setCoords] = useState({});

    const input_ref = React.createRef();
    var ymaps_provider;

    const handleSubmit = (event) => {
        event.preventDefault();
        const randomNumber = 10;
        setEstPrice(randomNumber);
        console.log("Rooms: ", rooms);
        console.log("Area: ", area);
        console.log("Floor: ", floor);
        console.log("Gas: ", gas);
        console.log("Elev: ", elev);
        console.log("Balcony: ", balcony);
        console.log("Address: ", address);
        // console.log(coords);
    }

    const onSuggestSelect = (e) => {
        const name = e.get("item").value;
        setAddress(name);
        ymaps_provider.geocode(name).then(result => {
            const coord = result.geoObjects.get(0).geometry.getCoordinates();
        //   setCoords(coord);
            console.log("Succes");
            console.log(coord[0]);
            console.log(coord[1]);
            setCoords({latitude: coord[0], longitude: coord[1]});
        });
      };

    const onYmapsLoad = ymaps => {
        ymaps_provider = ymaps;
        new ymaps_provider.SuggestView(input_ref.current, {width: 800}).events.add(
            "select",
            onSuggestSelect
        );
    };

    return (
        <div className="Estimator">
            <div className="Params">
                <form onSubmit={handleSubmit}>
                    <FormGroup>
                        <>
                            <H4> Количество комнат </H4>
                            <NumericInput name="rooms" className={ Classes.LARGE + ' ' + Classes.FILL } placeholder="0" value={rooms} onValueChange={(newValue) => setRooms(valid_number(newValue))} min={0}></NumericInput>
                        </>
                        <Divider className={ Classes.ELEVATION_0 }/>
                        <>
                            <H4> Площадь </H4>
                            <NumericInput name="area" className={ Classes.LARGE + ' ' + Classes.FILL } value={area} onValueChange={(newValue) => setArea(valid_number(newValue))} min={0}></NumericInput>
                        </>
                        <Divider className={ Classes.ELEVATION_0 }/>
                        <>
                            <H4> Этаж </H4>
                            <NumericInput name="floor" className={ Classes.LARGE + ' ' + Classes.FILL } value={floor} onValueChange={(newValue) => setFloor(valid_number(newValue))} min={0}></NumericInput>
                        </>
                        <Divider className={ Classes.ELEVATION_0 }/>
                        <>
                            <H4> Есть газ </H4>
                            <NumericInput name="gas" className={ Classes.LARGE + ' ' + Classes.FILL } value={gas} onValueChange={(newValue) => setGas(valid_boolean(newValue))} max={1} min={0} ></NumericInput>
                        </>
                        <Divider className={ Classes.ELEVATION_0 }/>
                        <>
                            <H4> Есть лифт </H4>
                            <NumericInput name="elev" className={ Classes.LARGE + ' ' + Classes.FILL } value={elev} onValueChange={(newValue) => setElev(valid_boolean(newValue))} max={1} min={0} ></NumericInput>
                        </>
                        <Divider className={ Classes.ELEVATION_0 }/>
                        <>
                            <H4> Есть балкон </H4>
                            <NumericInput name="balcony" className={ Classes.LARGE + ' ' + Classes.FILL } value={balcony} onValueChange={(newValue) => setBalcony(valid_boolean(newValue))} max={1} min={0} ></NumericInput>
                        </>
                        <Divider className={ Classes.ELEVATION_0 }/>
                        <>
                            <H4> Адрес </H4>
                            <InputGroup inputRef={input_ref} fill={true} name="address" value={address} onChange={(event) => setAddress(event.target.value)} disabled={ false }></InputGroup>
                        </>
                        <Divider className={ Classes.ELEVATION_0 }/>
                        <>
                            <Button text=" Оценить " fill={true} outlined={true} type="submit"></Button>
                        </>
                    </FormGroup>
                    <YMaps query={{ apikey: YMAPS_APIKEY, load: "package.full" }}>
                        <Map style={{visibility: "hidden"}} modules={['geocode']} state={mapState} onLoad={onYmapsLoad}>
                        </Map>
                    </YMaps>
                </form>
            </div>
            <Divider className={ Classes.VERTICAL }/>
            <div className="Estimate">
                <H4>
                    Оценочная стоимость
                </H4>
                <p className="Result">
                    { estPrice }
                </p>
            </div>
        </div>
    );
}