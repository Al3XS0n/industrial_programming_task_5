import "./Placement.css";
import { useNavigate } from "react-router";
import { Card, H5 } from "@blueprintjs/core";

export default function Placement(props, { navigation }) {
    const navigate = useNavigate();

    function handleClick() {
        console.log("Id: ", props.id);
        console.log("Name: ", props.name);
        console.log("Disc: ", props.disc);
        // navigate(`/placement_${props.id}`);
    }
    
    return (
        <div className="placement" id={"placement-" + props.id}>
            <Card interactive={true} onClick={ handleClick }>
                <H5> { props.name } </H5>
                <p> { props.disc }</p>
            </Card>
        </div>
    );
}