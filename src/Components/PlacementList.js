import "./PlacementList.css";
import Placement from "./Placement";
import { H3 } from "@blueprintjs/core";
import { useState } from "react";

// function GetPlacement() {

// }

export default function PlacementList() {
    const [placements, ] = useState([
        {id: 1, name:"Test placement 1", disc: "Some Description 1"},
        {id: 2, name:"Test placement 2", disc: "Some Description 2"},
        {id: 3, name:"Test placement 3", disc: "Some Description 3"}
    ]);

    return (
        <div className="placementList">
            <H3> Последние объявления </H3>
            {placements && placements.map(placement =>
                <Placement 
                    key={placement.id}
                    id={placement.id} 
                    name={placement.id}
                    disc={placement.disc}
                />

            )}
        </div>
    );
}