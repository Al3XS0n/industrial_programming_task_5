import 'jsdom-global/register';
import { mount } from "enzyme";
import React from "react";
import Navigation from '../src/Components/Navigation';
import Adapter from "enzyme-adapter-react-16";
import { shallow, configure } from 'enzyme';

configure({adapter: new Adapter()});
describe("Navigation", () => {
  const wrapper = shallow(<Navigation />);

  test("To match snapshot", () => {
    expect(wrapper).toMatchSnapshot();
  })

  test("Map anchor", () => {
    expect(wrapper.find('[href="/map"]').prop('href')).toBe('/map');
  });

  test("Calculator anchor", () => {
    expect(wrapper.find('[href="/calculator"]').prop('href')).toBe('/calculator');
  });

  test("Placements anchor", () => {
    expect(wrapper.find('[href="/placements"]').prop('href')).toBe('/placements');
  });
});