import 'jsdom-global/register';
import React from "react";
import Adapter from "enzyme-adapter-react-16";
import { mount } from "enzyme";
import { shallow, configure } from 'enzyme';
import PlacementList from "../src/Components/PlacementList";
import { BrowserRouter } from 'react-router-dom';

configure({adapter: new Adapter()});
describe("PlacementList", () => {
    const wrapper = mount(<BrowserRouter><PlacementList /></BrowserRouter>);

    test("Match snapshot", () => {
        expect(wrapper).toMatchSnapshot();
    });
});