import 'jsdom-global/register';
import { mount } from "enzyme";
import React from "react";
import PriceEstimator from "../src/Components/PriceEstimator";
import Adapter from "enzyme-adapter-react-16";
import { shallow, configure } from 'enzyme';

configure({adapter: new Adapter()});
describe("PriceEstimator", () => {
  const wrapper = mount(<PriceEstimator />);

  const rooms = wrapper.find("input[name='rooms']");
  const area = wrapper.find("input[name='area']");
  const floor = wrapper.find("input[name='floor']");
  const gas = wrapper.find("input[name='gas']");
  const elev = wrapper.find("input[name='elev']");
  const balcony = wrapper.find("input[name='balcony']");
  const address = wrapper.find("input[name='address']");

  const button = wrapper.find("button[type='submit']");

  test("7 logs", () => {
    const spy = jest.spyOn(global.console, 'log');
    button.simulate("submit");
    expect(global.console.log).toHaveBeenCalledTimes(7);
    spy.mockRestore();
  });

  test("Rooms=12", () => {
    const spy = jest.spyOn(global.console, 'log');
    rooms.simulate("change", { target: { value: "12" } });
    button.simulate("submit");
    expect(global.console.log).toHaveBeenNthCalledWith(1, "Rooms: ", 12);
    spy.mockRestore();
  });

  test("Area=26", () => {
    const spy = jest.spyOn(global.console, 'log');
    area.simulate("change", { target: { value: "26" } });
    button.simulate("submit");
    expect(global.console.log).toHaveBeenNthCalledWith(2, "Area: ", 26);
    spy.mockRestore();
  });

  test("Floor=3", () => {
    const spy = jest.spyOn(global.console, 'log');
    floor.simulate("change", { target: { value: "3" } });
    button.simulate("submit");
    expect(global.console.log).toHaveBeenNthCalledWith(3, "Floor: ", 3);
    spy.mockRestore();
  });

  test("Gas=1", () => {
    const spy = jest.spyOn(global.console, 'log');
    gas.simulate("change", { target: { value: "1" } });
    button.simulate("submit");
    expect(global.console.log).toHaveBeenNthCalledWith(4, "Gas: ", 1);
    spy.mockRestore();
  });

  test("Elev=1", () => {
    const spy = jest.spyOn(global.console, 'log');
    elev.simulate("change", { target: { value: "1" } });
    button.simulate("submit");
    expect(global.console.log).toHaveBeenNthCalledWith(5, "Elev: ", 1);
    spy.mockRestore();
  });

  test("Balcony=0", () => {
    const spy = jest.spyOn(global.console, 'log');
    balcony.simulate("change", { target: { value: "0" } });
    button.simulate("submit");
    expect(global.console.log).toHaveBeenNthCalledWith(6, "Balcony: ", 0);
    spy.mockRestore();
  });

  test("Balcony Incorect Value", () => {
    const spy = jest.spyOn(global.console, 'log');
    balcony.simulate("change", { target: { value: "-2" } });
    button.simulate("submit");
    expect(global.console.log).toHaveBeenNthCalledWith(6, "Balcony: ", 0);
    spy.mockRestore();
  });

  test("Address=Где то", () => {
    const spy = jest.spyOn(global.console, 'log');
    address.simulate("change", { target: { value: "Где-то" } });
    button.simulate("submit");
    expect(global.console.log).toHaveBeenNthCalledWith(7, "Address: ", "Где-то");
    spy.mockRestore();
  });
});