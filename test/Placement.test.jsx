import 'jsdom-global/register';
import { mount } from "enzyme";
import React from "react";
import { BrowserRouter, Router } from 'react-router-dom';
import Placement from '../src/Components/Placement';
import Adapter from "enzyme-adapter-react-16";
import { shallow, configure } from 'enzyme';

configure({adapter: new Adapter()});
describe("Placement", () => {
    const wrapper = mount(<BrowserRouter><Placement id={1} name="test" disc="tested" /></BrowserRouter>);

    test("Card is interactive", () => {
        const spy = jest.spyOn(global.console, 'log');
        expect(wrapper).toMatchSnapshot();
        wrapper.find('.bp4-card').simulate('click');
        expect(global.console.log).toHaveBeenCalledTimes(3);
        spy.mockRestore();
    });

    test("Correct Id", () => {
        const spy = jest.spyOn(global.console, 'log');
        wrapper.find('.bp4-card').simulate('click');
        expect(global.console.log).toHaveBeenNthCalledWith(1, "Id: ", 1);
        spy.mockRestore();
    });

    test("Correct Name", () => {
        const spy = jest.spyOn(global.console, 'log');
        wrapper.find('.bp4-card').simulate('click');
        expect(global.console.log).toHaveBeenNthCalledWith(2, "Name: ", "test");
        spy.mockRestore();
    });

    test("Correct Disc", () => {
        const spy = jest.spyOn(global.console, 'log');
        wrapper.find('.bp4-card').simulate('click');
        expect(global.console.log).toHaveBeenNthCalledWith(3, "Disc: ", "tested");
        spy.mockRestore();
    });
});