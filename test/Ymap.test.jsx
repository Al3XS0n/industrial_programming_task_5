import 'jsdom-global/register';
import React from "react";
import Adapter from "enzyme-adapter-react-16";
import { mount } from "enzyme";
import { shallow, configure } from 'enzyme';
import Ymap from '../src/Components/Ymap';
import { YMaps } from 'react-yandex-maps';
import { YMAPS_APIKEY } from '../src/Tools/API';

configure({adapter: new Adapter()});
describe("PlacementList", () => {
    const wrapper = mount(<YMaps query={{apikey: YMAPS_APIKEY}}><Ymap></Ymap></YMaps>);

    test("Match snapshot", () => {
        expect(wrapper).toMatchSnapshot();
    });
});